const steps = [
  {
    element: '.hamburger-container',
    popover: {
      title: '侧边栏',
      description: '打开或关闭侧边栏',
      position: 'bottom'
    }
  },
  {
    element: '.right-menu',
    popover: {
      title: '个人信息',
      description: '查看个人信息和退出登录',
      position: 'left'
    },
    padding: 0
  }
]

export default steps
