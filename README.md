# vue-admin-template

English | [简体中文](./README-zh.md)

> A minimal vue admin template with Element UI & axios & iconfont & permission control & lint

# 修改方式
全局修改
- LOGO在src/layout/components/Sidebar/Logo.vue
- 导航栏在src/layout/components/Navbar.vue

自定义页面
- 在src/views中写页面,可以分文件夹创建
- 在router/index.js中定义侧边栏,菜单图标可以在完整示例的图标中查询，并替换icon
- 然后开始写新页面
